<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <link rel="stylesheet" href="styles/main.css" />
        <link rel="stylesheet" href="css/index.css" />
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script> 
        <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed|Indie+Flower|Itim" rel="stylesheet">
    <body>
        <div class="top">Criticlick </div>
        <div class="reg"><a href="register.php">register</a> </div>
        <div class="websitename">Criticlick </div>
        <div class="login">
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?> 
        <div class="textinlogin">
        <form action="includes/process_login.php" method="post" name="login_form"> 			
            Email &nbsp;&nbsp;&nbsp;&nbsp;: <input type="text" name="email" /><br><br>
            Password: <input type="password" 
                             name="password" 
                             id="password"/>
                             <br><br>&nbsp;
            <input type="button" 
                   value="Login"
                   class="button" 
                   onclick="formhash(this.form, this.form.password);" /> 
        </form>
        
    </div>
    </div>
    </body>
</html>
